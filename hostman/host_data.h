#pragma once

#include <cstring>
#include <iostream>
#include <list>
#include <vector>
#include <windows.h>

struct HostLine {
	std::string ip;
	std::string domain;
	std::string comment;
	bool is_enable;
};

struct HostProject {
	bool is_enable;
	std::string name;
	std::vector<HostLine> lines;
};


std::string GetHostFilePath();
std::string GetFontPath();
HostLine ParseLine(std::string line);
void ParseHostFile(std::list<HostLine>* lines);
void WriteHostFile(std::list<HostLine>* lines);
std::string G2U(const std::string& gb2312);
void FlushDnsUsingDll();
void OpenHostFile();
float GetDPI();

std::wstring OpenFileDlg(HWND hwnd);

bool AddFromConfig(std::list<HostLine>* lines);
bool AddFromConfig(std::list<HostLine>* lines, const std::wstring& config);

void LoadProjects(std::list<HostProject>* projects);
void SaveProjects(std::list<HostProject>* projects);


