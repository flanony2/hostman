#include "host_data.h"

#include <windows.h>
#include <shellapi.h>
#include <fstream>
#include <sstream>
#include <regex>
#include "json.hpp"
using json = nlohmann::json;


bool IsIP(const std::string& ip)
{
	std::regex v4("^((25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.){3}(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)$");
	std::regex v6("^([\\da-fA-F]{1,4}:){7}[\\da-fA-F]{1,4}$");
	return regex_match(ip, v4) || regex_match(ip,v6);
}

bool IsDomain(const std::string& domain) {
	std::regex d("^(?=^.{3,255}$)[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+$");
	return regex_match(domain, d);
}

std::string U2G(const std::string& utf8)
{
	int len = MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), -1, NULL, 0);
	wchar_t* wstr = new wchar_t[len + 1];
	memset(wstr, 0, len + 1);
	MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), -1, wstr, len);
	len = WideCharToMultiByte(CP_ACP, 0, wstr, -1, NULL, 0, NULL, NULL);
	char* str = new char[len + 1];
	memset(str, 0, len + 1);
	WideCharToMultiByte(CP_ACP, 0, wstr, -1, str, len, NULL, NULL);
	if (wstr) delete[] wstr;
	std::string out(str);
	if (str) delete[] str;
	return out;
}
//GB2312到UTF-8的转换
std::string G2U(const std::string& gb2312)
{
	int len = MultiByteToWideChar(CP_ACP, 0, gb2312.c_str(), -1, NULL, 0);
	wchar_t* wstr = new wchar_t[len + 1];
	memset(wstr, 0, len + 1);
	MultiByteToWideChar(CP_ACP, 0, gb2312.c_str(), -1, wstr, len);
	len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);
	char* str = new char[len + 1];
	memset(str, 0, len + 1);
	WideCharToMultiByte(CP_UTF8, 0, wstr, -1, str, len, NULL, NULL);
	if (wstr) delete[] wstr;
	std::string out(str);
	if (str) delete[] str;
	return out;
}


std::string ReplaceAll(std::string str, const std::string& from, const std::string& to) {
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
	}
	return str;
}

std::string GetHostFilePath()
{
	char buf[MAX_PATH] = "";
	GetWindowsDirectoryA(buf, MAX_PATH);
	return std::string(buf).append("\\System32\\drivers\\etc\\hosts");
}

std::string GetFontPath()
{
	char buf[MAX_PATH] = "";
	GetWindowsDirectoryA(buf, MAX_PATH);
	return std::string(buf).append("\\Fonts\\msyh.ttc");
}

bool StartWith(const std::string& str, const std::string& with) {
	return (str.rfind(with, 0) == 0);
}

HostLine ParseLine(std::string line)
{
	line=ReplaceAll(line, "\t", " ");
	line = ReplaceAll(line, "\xEF\xBB\xBF", "");

	HostLine hl;

	int section = 0;
	for (size_t i = 0; i < line.length(); ++i) {
		if (line.at(i) == '#' && section!=3) {
			section = 3;
			continue;
		}

		switch (section) {
		case 0:
			if (line.at(i) != ' ') {
				section = 1;
				hl.ip.push_back(line.at(i));
			}
			break;
		case 1:
			if (line.at(i) != ' ') {
				hl.ip.push_back(line.at(i));
			}
			else {
				section = 2;
			}
			break;
		case 2:
			if (line.at(i) != '#') {
				if(line.at(i)!=' ') //skip space
					hl.domain.push_back(line.at(i));
			}
			else if(hl.domain.length()>0){
				section = 3;
			}
			break;
		case 3:
			hl.comment.push_back(line.at(i));
			break;
		}
	}

	hl.is_enable = true;
	if (hl.ip.length() <= 0 && hl.domain.length() <= 0)
	{
		std::string l = hl.comment;
		if (l.length() > 1 && l.at(0)!=' ') {
			HostLine try_line = ParseLine(l);
			if (IsIP(try_line.ip) && IsDomain(try_line.domain)) {
				hl.ip = try_line.ip;
				hl.domain = try_line.domain;
				hl.comment = try_line.comment;
				hl.is_enable = false;
			}
		}
	}

	return hl;
}

void ParseHostFile(std::list<HostLine>* lines)
{
	if (lines == nullptr) return;

	std::string host_path = GetHostFilePath();
	std::ifstream infile(host_path);

	std::string line;
	while (std::getline(infile, line))
	{
		lines->push_back(ParseLine(line));
	}
	infile.close();
}


void WriteHostFile(std::list<HostLine>* lines)
{
	if (lines == nullptr) return;

	std::string host_path = GetHostFilePath();
	std::ofstream outfile(host_path, std::ios::out | std::ios::trunc);

	for (std::list<HostLine>::iterator it = lines->begin(); it != lines->end(); ++it) {
		if (!it->is_enable) {
			outfile.write("#",1);
		}

		if (it->ip.length() > 0)
		{
			outfile << it->ip + "    ";
		}
		if (it->domain.length() > 0) {
			outfile << it->domain + "    ";
		}
		
		if (it->comment.length() > 0) {
			outfile << std::string("#") + it->comment;
		}
		
		outfile << std::endl;
	}

	outfile.close();

	FlushDnsUsingDll();
}


typedef BOOL(WINAPI *DnsFlushResolverCacheFuncPtr)();

void FlushDnsUsingDll() {
	HMODULE dnsapi = LoadLibrary(L"dnsapi.dll");
	if (dnsapi == NULL) {
		printf("Failed loading module: %d\n", GetLastError());
		return;
	}
	DnsFlushResolverCacheFuncPtr DnsFlushResolverCache = (DnsFlushResolverCacheFuncPtr)GetProcAddress(dnsapi, "DnsFlushResolverCache");
	if (DnsFlushResolverCache == NULL) {
		printf("Failed loading function: %d\n", GetLastError());
		FreeLibrary(dnsapi);
		return;
	}
	BOOL result = DnsFlushResolverCache();
	if (result) {
		printf("DnsFlushResolverCache succeeded\n");
	}
	else {
		printf("DnsFlushResolverCache succeeded: %d\n", GetLastError());
	}
	FreeLibrary(dnsapi);
}

void OpenHostFile()
{
	wchar_t buf[MAX_PATH] = L"";
	GetWindowsDirectoryW(buf, MAX_PATH);

	std::wstring notepad=std::wstring(buf).append(L"\\System32\\notepad.exe ");
	std::wstring param = std::wstring(buf).append(L"\\System32\\drivers\\etc\\hosts");

	SHELLEXECUTEINFOW sh;
	ZeroMemory(&sh, sizeof(SHELLEXECUTEINFO));
	sh.cbSize = sizeof(SHELLEXECUTEINFO);
	sh.lpFile = notepad.c_str();
	sh.lpParameters = param.c_str();
	sh.nShow = SW_SHOW;

	ShellExecuteExW(&sh);
}

std::wstring OpenFileDlg(HWND hwnd)
{

	OPENFILENAME ofn;
	wchar_t file[MAX_PATH];
	wchar_t file_title[MAX_PATH];
	file[0] = 0;
	file_title[0] = 0;
	memset(&ofn, 0, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = L"Hostman Profile(*.hman)\0*.hman;\0\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = file;
	ofn.nMaxFile = sizeof(file);
	ofn.lpstrFileTitle = file_title;
	ofn.nMaxFileTitle = sizeof(file_title);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_EXPLORER;
	//当按下确定按钮
	if (GetOpenFileName(&ofn))
	{
		return std::wstring(file);
	}
	else
	{
		return std::wstring();
	}

}

bool AddFromConfig(std::list<HostLine>* lines)
{
	std::wstring file_name=OpenFileDlg(0);
	if (file_name.empty()) return false;
	return AddFromConfig(lines,file_name);
}

bool AddFromConfig(std::list<HostLine>* lines, const std::wstring& config)
{
	std::ifstream infile(config);
	if (infile.is_open()) {
		std::string raw((std::istreambuf_iterator<char>(infile)),
			std::istreambuf_iterator<char>());

		
		json j3 = json::parse(raw);
		for (json::iterator it = j3.begin(); it != j3.end(); ++it) {
			
			if ((*it).find("ip") != (*it).end() 
				&& (*it).find("domain") != (*it).end()) {
				HostLine hl;

				hl.ip = (*it)["ip"].get<std::string>();
				hl.domain = (*it)["domain"].get<std::string>();
				hl.is_enable = true;
				
				if ((*it).find("domain") != (*it).end()) {
					hl.comment = (*it)["comment"].get<std::string>();
				}

				lines->push_back(hl);
			}
		}

		return true;
	}
	return false;
}


void LoadProjects(std::list<HostProject>* projects)
{
	//todo
}
void SaveProjects(std::list<HostProject>* projects)
{
	//todo
}

float GetDPI()
{
	int screen_w = ::GetSystemMetrics(SM_CXSCREEN);
	int screen_h = ::GetSystemMetrics(SM_CYSCREEN);
	HWND hwd = ::GetDesktopWindow();
	HDC hdc = ::GetDC(hwd);
	int width = ::GetDeviceCaps(hdc, DESKTOPHORZRES);
	int height = ::GetDeviceCaps(hdc, DESKTOPVERTRES);
	float scale = (float)width / screen_w;
	return scale;
}